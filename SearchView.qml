import QtQuick 2.15
import QtQuick.Controls 2.15

import com.ds.SymbolSearch 1.0

Page {
    SymbolSearch {
        id: search
        onSearchFinished: {
            searchResultList.model = search.getResults();
            busy.visible = false
            busy.running = false
            searchField.focus = false
        }
    }

    function searchForSymbol()
    {
        searchResultList.model = []
        busy.visible = true
        busy.running = true
        search.search(searchField.text);
    }

    function searchFieldFocusActive()
    {
        searchField.focus = true
    }

    Rectangle {
        id: recRow
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: 50
        color: "#222222"

        TextField {
            id: searchField
            focus: true
            placeholderText: "Search Keywords"
            placeholderTextColor: "#777"
            color: "#ffffff"
            verticalAlignment: Text.AlignVCenter
            hoverEnabled: false
            font.styleName: "Bold"
            layer.wrapMode: ShaderEffectSource.ClampToEdge
            bottomPadding: 0
            topPadding: 0
            anchors.rightMargin: 5
            height: 35
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.right: searchBtn.left
            anchors.margins: 5
            font.pixelSize: 20
            font.bold: true
            cursorVisible: true
            leftPadding: 10
            activeFocusOnPress: true

            onPressed: {
                searchField.focus = true
                clearSearchBtn.visible = true
            }
            onPressAndHold: {
                searchField.selectAll()
            }

            mouseSelectionMode: "SelectWords"

            persistentSelection: true
            onEditingFinished: {
                searchResultList.model = []
                searchForSymbol()
            }

            Keys.onReturnPressed: searchField.editingFinished()

            Button {
                id: clearSearchBtn
                width: 50
                height: parent.height - 7
                anchors.right: parent.right
                anchors.rightMargin: 10
                anchors.verticalCenter: parent.verticalCenter
                text: "X"
                visible: (searchField.text === "" || !searchField.focus) ? false : true
                onClicked: {
                    searchField.clear()
                    searchField.focus = false
                    clearSearchBtn.visible = false
                }
            }
        }

        Button {
            id: searchBtn
            height: parent.height
            width: 100
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: 0
            background: Rectangle {
                anchors.fill: parent
                color: "#00aaff"
            }

            Text {
                anchors.centerIn: parent
                text: "Search"
                color: "#000"
            }

            onClicked: searchForSymbol()
        }
    }

    ListView {
        id: searchResultList
        anchors.top: recRow.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        spacing: 5
        clip: true
        model: []
        delegate: Rectangle {
            width: parent.width
            height: 40
            color: "transparent"
            Text {
                id: stockSymbol
                width: 100
                text: modelData["1. symbol"]
                color: "white"
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 5
            }
            Text {
                id: companyName
                width: 250
                text: modelData["2. name"]
                color: "white"
                anchors.left: stockSymbol.right
                anchors.right: searchResultList.width > 700 ? type.left : addSymbolToListBtn.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: 5
                anchors.rightMargin: 15
                clip: true
            }
            Text {
                id: type
                width: 100
                text: modelData["3. type"]
                color: "white"
                anchors.right: addSymbolToListBtn.left
                anchors.verticalCenter: parent.verticalCenter
                clip: true
                visible: searchResultList.width > 700
            }
            Button {
                id: addSymbolToListBtn
                width: 50
                text: "+"
                font.bold: true
                font.pixelSize: 20
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.rightMargin: 10
                height: parent.height
                background: Rectangle {
                    color: "#333333"
                    anchors.fill: parent
                    opacity: 1
                }
                onClicked: {
                    portfolio.appendSymbol(stockSymbol.text);
                    swipeView.currentIndex -= 1;
                }
            }
        }
    }

    // Add Popup here.

    BusyIndicator {
        id: busy
        width: 75
        height: 75
        anchors.centerIn: parent
        visible: false
        running: false
    }
}
