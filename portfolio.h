#ifndef PORTFOLIO_H
#define PORTFOLIO_H

#include <QObject>
#include <QDebug>
#include <QString>
#include <QVariant>
#include <QStringList>
#include <QMap>
#include <QFile>

#include <alphavantageapi.h>


class Portfolio : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString currentList READ currentList WRITE setCurrentList NOTIFY currentListChanged)
public:
    explicit Portfolio(QObject *parent = nullptr);
    ~Portfolio();
    Q_INVOKABLE void newList(const QString&);
    Q_INVOKABLE void newList(const QString&,
                              const QStringList&);
    Q_INVOKABLE void removeList(const QString&);
    Q_INVOKABLE void appendSymbol(const QString&);
    Q_INVOKABLE void removeSymbol(const QString&);
    Q_INVOKABLE QList<QVariant> getPortfolioValues();
    Q_INVOKABLE QStringList getPortfolioKeys();
    Q_INVOKABLE int getPortfolioSize();
    QString currentList();
    void setCurrentList(const QString&);
    Q_INVOKABLE void setCurrentListIndex(const int&);

signals:
    void listAdded();
    void listUpdated();
    void listRemoved();
    void symbolAdded();
    void symbolRemoved();
    void currentListChanged();

private:
    AlphaVantageAPI *alpha; // Network facilities
    QString selectedList;
    QVariantMap *data;
};

#endif // PORTFOLIO_H
