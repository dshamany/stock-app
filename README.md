# Stocks App

## 0.0.8
- Completed Basic Functionality
- Persistent Storage still missing
- Add new stock symbols to current selected list

## 0.0.7
- New Interface / Responsive Design
- Ability to add themes in the future
- Simpler code structure
- Gain/Loss now changes the tile background for easy viewing
- Multi-Portfolio support added

## 0.0.6
- One-Click search functions properly
- UI adjusted
- Added ability to search for symbols (not yet implemented in the UI)
- UI improvements to help the app become more professional

## 0.0.5
- Code refactored to utilize Qt facilities better
- Bug: Press search twice
- Completely rewrote Symbol Class

## 0.0.4
- Bug where some data does not populate and requires refresh
- Added ScrollView, however, it's incomplete (sorry small phones)

## 0.0.3
- Code clean up
- Added all symbol indicators
- Redesigned symbol view
- Compartmentalized UI

## 0.0.2
- Code Clean Up
- Added growth indicator
- Adjusted styling

## 0.0.1
- Enter Symbol to retrieve price
- Platform Independent (Can be crosscompiled to work on all platforms)
- Material Theme (will change in the future)
- Uses Alphavantage's free API

### Plan
- Search for symbol by company name (function implemented, UI is next)
- Create new portfolio groups (done, not polished)
- Add new symbol to portfolio (dependent on `findSymbol()` implementation)
- Add pintch to zoom capability for fluid UI experience
- Add persistent storage

- Switch to Qt 6 by 2022 the earliest (due to compatibility issues)
