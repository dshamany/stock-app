#include "symbol.h"

Symbol::Symbol() : alpha(new AlphaVantageAPI(this))
{}

bool Symbol::hasData()
{
    return !data.isEmpty();
}

QString Symbol::getKeyValue(const QString& key)
{
    return data[key].toString();
}

QMap<QString, QString> Symbol::findSymbol(const QString &keyword)
{
    alpha->findSymbol(keyword);
    this->connect(alpha,
                  &AlphaVantageAPI::symbolsFound, this,
                  [this]{
        auto mData = QJsonDocument::fromJson(this->alpha->data()).toVariant().toMap()["bestMatches"].toList();
        QMap<QString, QString> foundList = {};
        for (auto & item : mData)
        {
            foundList.insert(item.toJsonObject().toVariantMap()["1. symbol"].toString(),
                             item.toJsonObject().toVariantMap()["2. name"].toString());
        }
        emit dataChanged();
        return foundList;
    });
    return QMap<QString, QString>();
}

void Symbol::globalQuote(const QString& symbol)
{
    alpha->globalQuote(symbol);

    this->connect(alpha,
                  &AlphaVantageAPI::dataArrived, this,
                  [this]{
        auto json = QJsonDocument::fromJson(alpha->data());
        setData(json["Global Quote"].toVariant().toMap());
    });
}


void Symbol::setData(QMap<QString, QVariant> newData)
{
    data = newData;
    emit dataChanged();
}





