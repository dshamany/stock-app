#include "filter.h"

StockList asc(StockList& sl, const QString&key)
{
    std::sort(sl.stocks().begin(), sl.stocks().end(), [&](auto a, auto b){
         return a[key] < b[key];
    });
}

StockList dec(StockList& sl, const QString&key)
{
    std::sort(sl.stocks().begin(), sl.stocks().end(), [&](auto a, auto b){
         return a[key] > b[key];
    });
}
