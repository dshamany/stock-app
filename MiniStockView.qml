import QtQuick 2.0
import QtQuick.Controls 2.15
import com.ds.Symbol 1.0

Rectangle {
    id: recRoot
    property string symbol: "";
    property color textColor: "white"
    property int gainLoss: 0

    anchors.margins: 1
    width: 100
    height: 100
    color: gainLoss < 0 ? "red" : "lightgreen"
    border.width: 1
    border.color: "#777"

    Symbol {
        id: symObj
        onDataChanged: {
            priceInfo.text  = symObj.getKeyValue("05. price") ? ("$" + Number(symObj.getKeyValue("05. price"))) : "N/A";
            gainLoss = Number(symObj.getKeyValue("05. price")) - Number(symObj.getKeyValue("02. open"));
            busy.visible = false
            busy.running = false
        }
    }

    Component.onCompleted: {
        symObj.globalQuote(symbol);
        symObj.dataChanged();
    }

    Text {
        id: symbolInfo
        color: recRoot.color === "red" ? "white" : "black"
        font.bold: true
        font.pixelSize: 20
        text: symbol
        anchors.left: parent.left
        anchors.right: parent.right
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment:  Text.AlignVCenter
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        anchors.top: parent.top
        height: parent.height / 2
        width: parent.width
        visible: !busy.running
    }
    Text {
        id: priceInfo
        color: recRoot.color === "red" ? "white" : "black"
        font.pixelSize: 20
        text: "-"
        anchors.left: parent.left
        anchors.right: parent.right
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment:  Text.AlignVCenter
        anchors.top: symbolInfo.bottom
        anchors.bottom: parent.bottom
        height: parent.height / 2
        width: parent.width
        visible: !busy.running
    }

    BusyIndicator {
        id: busy
        anchors.centerIn: parent
        visible: true
        running: true
        z: 10
    }

    MouseArea {
        id: updateBtn
        anchors.fill: parent
        onClicked: {
            busy.running = true
            busy.visible = true
            symObj.globalQuote(symbol)
        }
    }
}


