#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QMap>
#include <QQmlContext>

#include "alphavantageapi.h"
#include "symbol.h"
#include "portfolio.h"
#include "symbolsearch.h"

int main(int argc, char *argv[])
{
    // Register Custom Objects
    qmlRegisterType<Symbol>("com.ds.Symbol", 1, 0, "Symbol");
    qmlRegisterType<Portfolio>("com.ds.Portfolio", 1, 0, "Portfolio");
    qmlRegisterType<SymbolSearch>("com.ds.SymbolSearch", 1, 0, "SymbolSearch");

    // Depreciated in Qt6
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    // Create application object
    QGuiApplication app(argc, argv);

    // Create rendering engine
    QQmlApplicationEngine engine;

    // Set UI URL
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    // Use Signals and Slots to connect application to engine
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection); // Not sure why
    // Load UI
    engine.load(url);

    // Maintain Event Loop for program to remain on
    return app.exec();
}
