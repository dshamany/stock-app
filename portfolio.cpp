#include "portfolio.h"

Portfolio::Portfolio(QObject *parent)
    : QObject(parent),
      alpha(new AlphaVantageAPI()),
      data(new QVariantMap())
{
    QFile file("portfolioData.json");

    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Failed to Open File\r\n";
    }
    else if (file.isOpen())
    {
        auto json = QJsonDocument::fromJson(file.readAll());
        *data = json.toVariant().toMap();

        if (json.isEmpty())
        {
            this->selectedList = "Default";
            data->insert(this->selectedList, {});

        }
    }
}

Portfolio::~Portfolio()
{
    QFile file("portfolioData.json");
    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug() << "Failed to Open File\r\n";
        exit(1);
    }
    auto jsonObj = QJsonObject::fromVariantMap(*data);
    QJsonDocument jsonDoc(jsonObj);
    auto jsonCompact = jsonDoc.toJson(QJsonDocument::Compact);
    file.write(jsonCompact);
    file.close();
}

void Portfolio::newList(const QString &listName)
{
    data->insert(listName, {});
    setCurrentList(listName);
    emit listAdded();
}

void Portfolio::newList(const QString& listName,
                        const QStringList& content)
{
    data->insert(listName, content);
    setCurrentList(listName);
    emit listAdded();
}

void Portfolio::removeList(const QString &listName)
{
    data->remove(listName);
    emit listRemoved();
}

void Portfolio::appendSymbol(const QString &symbol)
{
    auto portfolioList = data->value(selectedList).toList();
    portfolioList.append(symbol);
    data->insert(selectedList, portfolioList);
    emit symbolAdded();
}

void Portfolio::removeSymbol(const QString &symbol)
{
    auto portfolioList = data->value(selectedList).toList();
    auto idx = portfolioList.indexOf(symbol);
    portfolioList.removeAt(idx);

    emit symbolRemoved();
}

QList<QVariant> Portfolio::getPortfolioValues()
{
    auto result = data->value(selectedList).toList();
    return result;
}

QStringList Portfolio::getPortfolioKeys()
{
    auto result = data->keys();
    return result;
}

int Portfolio::getPortfolioSize()
{
    return data->value(selectedList).toList().size();
}

QString Portfolio::currentList()
{
    return selectedList;
}

void Portfolio::setCurrentList(const QString &listName)
{
    selectedList = listName;
    emit currentListChanged();
}

void Portfolio::setCurrentListIndex(const int &index)
{
    selectedList = data->keys()[index];
}

