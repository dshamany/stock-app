import QtQuick 2.0
import QtQuick.Controls 2.15

Page {
    id: portfolioRoot

    property var gridViewModel: stocksGrid.model

    Rectangle {
        id: listSelector
        width: parent.width
        height: 50
        anchors.top: parent.top
        anchors.left: parent.left
        color: "#222"

        ComboBox {
            id: portfolioSelector
            editable: true
            onCurrentIndexChanged: {
                portfolio.setCurrentListIndex(currentIndex)
            }

            onAccepted: {
                if (find(editText) === -1)
                {
                    portfolio.newList(editText);
                    portfolioSelector.model = portfolio.getPortfolioKeys()
                    while (portfolioSelector.currentIndex < portfolioSelector.count - 1)
                        portfolioSelector.incrementCurrentIndex()
                }
            }

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: 3
            anchors.leftMargin: 3
            anchors.verticalCenter: parent.verticalCenter
            width: 175
            height: parent.height + 5
            model: portfolio.getPortfolioKeys()
            font.pixelSize: 16
            currentIndex: 0
            onCurrentValueChanged: {
                gridViewModel = portfolio.getPortfolioValues()
            }
        }
    }

    Flickable {
        id: scroll
        width: parent.width
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: listSelector.bottom
        clip: true

        Component {
            id: miniStockView
            MiniStockView {
                symbol: modelData;
            }

        } // Component

        GridView {
            id: stocksGrid
            anchors.fill: parent
            anchors.topMargin: 5
            model: gridViewModel
            delegate: miniStockView
            onModelChanged: {
                update()
            }
        } // stockGrid

    } // ScrollView

} // Page

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
