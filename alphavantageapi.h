#ifndef ALPHAVANTAGEAPI_H
#define ALPHAVANTAGEAPI_H

#include <QGuiApplication>
#include <QObject>
#include <QMap>

#include <QJsonDocument>
#include <QJsonObject>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include <functional>

class AlphaVantageAPI : public QObject
{
    Q_OBJECT
public:
    explicit AlphaVantageAPI(QObject *parent = nullptr);
    void setBaseUrl(const QString&);
    QString baseUrl() const;
    QString fullUrl() const;
    void setApiKey(const QString&);
    QString apiKey() const;
    QString symbol() const;
    void setSymbol(const QString&);
    void setParameter(const QString&,
                      const QString&);
    void replaceParameter(const QString&,
                          const QString&);
    void removeParameter(const QString&);
    void parameterList(const QMap<QString, QString>&);
    QMap<QString, QString> parameterList();
    QByteArray data() const;
    void findSymbol(const QString&);
    void globalQuote(const QString &);

signals:
    void parameterChanged();
    void baseUrlChanged();
    void dataArrived();
    void symbolsFound();

private:
    QMap<QString,QString> parameters;
    QString base_url;
    QNetworkAccessManager *man;
    QNetworkRequest req;
    QNetworkReply *reply;
    QString api_key;
    QString sym;
    QByteArray read;
};

#endif // ALPHAVANTAGEAPI_H
