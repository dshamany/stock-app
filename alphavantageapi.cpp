#include "alphavantageapi.h"

AlphaVantageAPI::AlphaVantageAPI(QObject *parent) : QObject(parent)
{
    man = new QNetworkAccessManager(this);
    this->setBaseUrl("https://www.alphavantage.co/query/?");
    this->setApiKey("7T40FQ98W7AC8GE6");
}

void AlphaVantageAPI::setParameter(const QString &key,
                                   const QString &val)
{
    parameters[key] = val;
}

void AlphaVantageAPI::replaceParameter(const QString &key,
                                       const QString& value)
{
    parameters[key] = value;
}

void AlphaVantageAPI::removeParameter(const QString &key)
{
    parameters.remove(key);
}

void AlphaVantageAPI::parameterList(const QMap<QString, QString> &map)
{
    parameters.clear();
    auto keys = map.keys();
    for (auto &k : keys)
    {
        parameters[k] = map[k];
    }
}


QMap<QString, QString> AlphaVantageAPI::parameterList()
{
    return this->parameters;
}

QByteArray AlphaVantageAPI::data() const
{
    return read;
}

void AlphaVantageAPI::setBaseUrl(const QString &url)
{
    base_url = url; void setBaseUrl(const QString&);
}

QString AlphaVantageAPI::baseUrl() const
{
    return base_url;
}

QString addKeyValue(const QString &key,
                    const QString& val)
{
    return "&"+key+"="+val;
}

QString AlphaVantageAPI::fullUrl() const
{
    QString result = base_url;
    auto params = parameters.keys();
    for (auto &p : params)
    {
        result.append(addKeyValue(p, parameters[p]));
    }

    return result;
}

void AlphaVantageAPI::setApiKey(const QString &key)
{
    api_key = key;
}

QString AlphaVantageAPI::apiKey() const
{
    return api_key;
}

QString AlphaVantageAPI::symbol() const
{
    return this->sym;
}

void AlphaVantageAPI::setSymbol(const QString &symbol)
{
    this->sym = symbol;
}

void AlphaVantageAPI::findSymbol(const QString& keyword)
{

    QMap<QString, QString> params = {
        {"function", "SYMBOL_SEARCH"},
        {"apikey", this->apiKey()},
        {"keywords", keyword}
    };
    this->parameterList(params);


    this->req.setUrl(this->fullUrl());
    this->reply = man->get(req);

    this->connect(this->reply,
                  &QNetworkReply::finished,
                  this,
                  [=](){

        read = this->reply->readAll();
        emit symbolsFound();
    });
}

void AlphaVantageAPI::globalQuote(const QString& symbol)
{
    if (symbol != "")
    {
        this->setSymbol(symbol);
    }

    QMap<QString, QString> params = {
        {"apikey", this->api_key},
        {"symbol", this->sym},
        {"function", "GLOBAL_QUOTE"}
    };
    this->parameterList(params);

    this->req.setUrl(this->fullUrl());
    this->reply = man->get(req);

    this->connect(this->reply, &QNetworkReply::finished, this, [=](){
        this->read = this->reply->readAll();
        emit dataArrived();
    });
}

