import QtQuick 2.15
import QtQuick.Controls 2.15

import com.ds.Portfolio 1.0

ApplicationWindow {
    id: window
    width: 640
    minimumWidth: 400
    height: 480
    visible: true
    title: qsTr("StockApp")

    Portfolio {
        id: portfolio
        onSymbolAdded: {
            portfolioView.gridViewModel = portfolio.getPortfolioValues()
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        onCurrentIndexChanged: {
            tabBar.currentIndex = swipeView.currentIndex
        }

        PortfoliosView {
            id: portfolioView
        }

        SearchView {
            id: searchView
        }

        Page {
            Text {
                text: "Settings"
                color: "white"
                anchors.centerIn: parent
            }
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        height: 50
        onCurrentIndexChanged: {
            swipeView.currentIndex = tabBar.currentIndex
        }

        TabButton {
            text: "Portfolio"
        }

        TabButton {
            text: "Search"
        }

        TabButton {
            text: "Settings"
        }
    }

}
