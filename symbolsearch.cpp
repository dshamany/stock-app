#include "symbolsearch.h"

SymbolSearch::SymbolSearch(QObject *parent)
    : QObject(parent),
      alpha(new AlphaVantageAPI),
      searchResults({})
{
}

void SymbolSearch::search(const QString &keyword)
{
    alpha->findSymbol(keyword);

    connect(alpha, &AlphaVantageAPI::symbolsFound, this, [this](){
        auto json = QJsonDocument::fromJson(alpha->data());
        setResults(json["bestMatches"].toVariant().toList());
    });
}

QVariantList SymbolSearch::getResults()
{
    return searchResults;
}

void SymbolSearch::setResults(const QVariantList &results)
{
    searchResults = results;
    emit searchFinished();
}
