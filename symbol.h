#ifndef SYMBOL_H
#define SYMBOL_H

#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QObject>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include <functional>

#include "alphavantageapi.h"

class Symbol : public QObject
{
    Q_OBJECT
public:
    Symbol();
    Q_INVOKABLE bool hasData();
    Q_INVOKABLE QString getKeyValue(const QString&);
    Q_INVOKABLE QMap<QString, QString> findSymbol(const QString&);
    Q_INVOKABLE void globalQuote(const QString& symbol);
    void setData(QMap<QString, QVariant>);
signals:
    void dataChanged();

private:
    AlphaVantageAPI *alpha;
    QMap<QString, QVariant> data;
};

#endif // SYMBOL_H
