#ifndef FILTER_H
#define FILTER_H

#include <QVector>
#include <QMap>
#include <QString>
#include <QtAlgorithms>

#include "stocklist.h"

class Filter
{
public:
    Filter() = delete;
    Filter(const Filter&) = delete;
    ~Filter() = delete;
    static StockList asc(StockList&, const QString&);
    static StockList dec(StockList&, const QString&);

    enum Type {ASC, DEC};
};

#endif // FILTER_H
