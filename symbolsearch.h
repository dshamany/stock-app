#ifndef SYMBOLSEARCH_H
#define SYMBOLSEARCH_H

#include <QObject>
#include <QVariant>
#include <QString>

#include "alphavantageapi.h"

class SymbolSearch : public QObject
{
    Q_OBJECT
public:
    explicit SymbolSearch(QObject *parent = nullptr);
    Q_INVOKABLE void search(const QString&);
    Q_INVOKABLE QVariantList getResults();
    void setResults(const QVariantList&);

signals:
    void searchFinished();

private:
    AlphaVantageAPI* alpha;
    QVariantList searchResults;


};

#endif // SYMBOLSEARCH_H
